# README #

### Uporaba ###
* s klikom na gumb "nariši", se nariše naključna slika
* v vnosnih poljih za širino in višino lahko spremenimo vrednosti in ob kliku na gumb "spremeni velikost" spremenimo velikost slike, ki se bo narisala
* ko se slika do konca nariše, lahko izberemo nastavitve za barvo slike
* sliko lahko shranimo z gumbom "shrani"

### Več informacij ###
* opis programa: [http://math.andrej.com/2010/04/21/random-art-in-python/](http://math.andrej.com/2010/04/21/random-art-in-python/)
* Jure Hostnik