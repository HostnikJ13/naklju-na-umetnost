package Operatorji;

public class Boing2 extends Operator{
	Operator arg;
	
	private double boing2(double x) {	
		return Math.sin(25 * x) / Math.pow(2, (x * x));
	}
	
	public Boing2(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = {boing2(b[0]), boing2(b[1]), boing2(b[2])};
		return nova;
	}
}
