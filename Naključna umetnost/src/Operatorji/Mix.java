package Operatorji;

import java.util.Random;

public class Mix extends Operator{
	Operator w, arg1, arg2;
	
	public Mix(int velikost) {
		Random r = new Random();
		int i = r.nextInt(velikost + 1);
		int j = r.nextInt(velikost + 1);
		int k = Math.min(i, j);
		int l = Math.max(i, j);
		w = Operator.naredi(k);
		arg1 = Operator.naredi(l - k);
		arg2 = Operator.naredi(velikost - l);
	}
	
	public double[] izracunaj(double x, double y){
		double m = 0.5 * (w.izracunaj(x,  y)[0] + 1);
		double[] b1 = arg1.izracunaj(x,  y);
		double[] b2 = arg2.izracunaj(x,  y);
		double[] nova = new double[3];
		for (int i = 0; i < nova.length; i += 1){
			nova[i] = m * b1[i] + (1 - m) * b2[i];
		}
		return nova;
	}
}
