package Operatorji;

import java.util.Random;

public class Sin extends Operator{
	Operator arg;
	Random r = new Random();
	double f = Math.PI * r.nextDouble();
	double z = 1 + 5 * r.nextDouble();
	
	public Sin(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = new double[3];
		for (int i = 0; i < nova.length; i += 1){
			nova[i] = Math.sin(f + z * b[i]);
		}
		return nova;
	}
}
