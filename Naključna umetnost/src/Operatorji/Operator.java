package Operatorji;

import java.util.Random;


public abstract class Operator {
	
	public Operator() {
		super();
	}
	
	/**
	 * Ustvari naključen izraz iz "velikost" operatorjev.
	 */
	public static Operator naredi(int velikost) {
		Random r = new Random();
		
		if (velikost <= 0) {
			int i = r.nextInt(5) + 1;
			if (i == 1){
				return new Konst();
			}
			if (i == 2){
				return new SprX();
			}
			if (i == 3){
				return new SprY();
			}
			if (i == 4){
				return new XplusY();
			}
			if (i == 5){
				return new XkratY();
			}
		}
		
		int i = r.nextInt(13) + 1;
		if (i == 1){
//			System.out.print("Well ");
			return new Well(velikost - 1);
		}
		else if (i == 2){
//			System.out.print("Sin ");
			return new Sin(velikost - 1);
		}
		else if (i == 3){
//			System.out.print("Pr ");
			return new Produkt(velikost - 1);
		}
		else if (i == 4){
//			System.out.print("St ");
			return new Stopnja(velikost - 1);
		}
		else if (i == 5){
//			System.out.print("Mix ");
			return new Mix(velikost - 1);
		}
		else if (i == 6){
//			System.out.print("Tent ");
			return new Tent(velikost - 1);
		}
		else if (i == 7){
//			System.out.print("Na3 ");
			return new Na3(velikost - 1);
		}
//		else if (i == 8){
//			System.out.print("Tan ");
//			return new Tan(velikost - 1);
//		}
		else if (i == 8){
//			System.out.print("Cos2 ");
			return new Cos2(velikost - 1);
		}
		else if (i == 9){
//			System.out.print("aSin ");
			return new ArcSin(velikost - 1);
		}
//		else if (i == 11){
//			return new Mod(velikost - 1);
//		}
		else if (i == 10){
//			System.out.print("Boing ");
			return new Boing(velikost - 1);
		}
		else if (i == 11){
//			System.out.print("Boing2 ");
			return new Boing2(velikost - 1);
		}
		else if (i == 12){
//			System.out.print("Boing3 ");
			return new BoingBlack(velikost - 1);
		}
		else {
//			System.out.print("Vs ");
			return new Vsota(velikost - 1);
		}
	}
	
	/**
	 * Vsak operator izračuna in vrne vrednost glede na x, y iz [-1, 1].
	 */
	public abstract double[] izracunaj(double x, double y);
}
