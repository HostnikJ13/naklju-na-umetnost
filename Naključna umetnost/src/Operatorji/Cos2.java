package Operatorji;

import java.util.Random;

public class Cos2 extends Operator{
	Operator arg1, arg2, arg3;
	
	public Cos2(int velikost) {
		Random r = new Random();
		int i = r.nextInt(velikost + 1);
		int j = r.nextInt(velikost + 1);
		int k = Math.min(i, j);
		int l = Math.max(i, j);
		arg1 = Operator.naredi(k);
		arg2 = Operator.naredi(l - k);
		arg3 = Operator.naredi(velikost - l);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b1 = arg1.izracunaj(x, y);
		double[] b2 = arg2.izracunaj(x, y);
		double[] b3 = arg3.izracunaj(x, y);
		double[] nova = new double[3];
		for (int i = 0; i < nova.length; i += 1){
			nova[i] =  (Math.pow(Math.cos(b1[i]), 2) + Math.pow(Math.cos(b2[i]), 2) + Math.pow(Math.cos(b3[i]), 2)) / 3;
		}
		return nova;
	}
}
