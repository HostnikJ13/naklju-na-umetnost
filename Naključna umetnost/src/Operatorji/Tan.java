package Operatorji;

import java.util.Random;

public class Tan extends Operator{
	Operator arg;
	Random r = new Random();
	double k = r.nextDouble();
	
	public Tan(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = new double[3];
		for (int i = 0; i < nova.length; i += 1){
			nova[i] = k * Math.tan(b[i]);
		}
		return nova;
	}
}
