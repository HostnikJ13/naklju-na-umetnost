package Operatorji;

public class Tent extends Operator{
	Operator arg;
	
	public double tent(double x) {
		return 1 - 2 * Math.abs(x);
	}
	
	public Tent(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = {tent(b[0]), tent(b[1]), tent(b[2])};
		return nova;
	}
}
