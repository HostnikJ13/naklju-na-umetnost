package Operatorji;

public class Well extends Operator{
	Operator arg;
	
	public double well(double x) {
		return 1 - 2 / Math.pow(1 + x*x, 8);
	}
	
	public Well(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = {well(b[0]), well(b[1]), well(b[2])};
		return nova;
	}
}
