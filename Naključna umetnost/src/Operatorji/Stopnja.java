package Operatorji;

import java.util.Random;

public class Stopnja extends Operator{
	Operator st, arg1, arg2;
	Random r = new Random();
	double m = -1 + 2 * r.nextDouble();
	
	public Stopnja(int velikost) {
		int i = r.nextInt(velikost + 1);
		int j = r.nextInt(velikost + 1);
		int k = Math.min(i, j);
		int l = Math.max(i, j);
		st = Operator.naredi(k);
		arg1 = Operator.naredi(l - k);
		arg2 = Operator.naredi(velikost - l);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b1 = st.izracunaj(x,  y);
		double[] b2 = arg1.izracunaj(x,  y);
		double[] b3 = arg2.izracunaj(x,  y);
		double[] nova = new double[3];
		for (int i = 0; i < nova.length; i += 1){
			if (b1[i] < m){
				nova[i] = b2[i];
			}
			else {
				nova[i] = b3[i];
			}
		}
		return nova;
	}
}
