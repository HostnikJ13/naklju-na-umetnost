package Operatorji;

/**
 * Od tukaj prihaja črna barva. ^^
 */
public class BoingBlack extends Operator{
	Operator arg;
	
	private double boingBlack(double x) {		
		if (Math.abs(x) < 0.125){
		return 10 - Math.pow(24*x, 2);
		}
		return Math.abs(Math.sin(25 * x) / (x * x));
	}
	
	public BoingBlack(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = {boingBlack(b[0]), boingBlack(b[1]), boingBlack(b[2])};
		return nova;
	}
}
