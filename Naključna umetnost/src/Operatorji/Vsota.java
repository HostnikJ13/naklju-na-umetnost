package Operatorji;

import java.util.Random;

public class Vsota extends Operator{
	Operator arg1, arg2;
	
	public Vsota(int velikost) {
		Random r = new Random();
		int i = r.nextInt(velikost + 1);
		arg1 = Operator.naredi(velikost - i);
		arg2 = Operator.naredi(i);
	}
	public double[] izracunaj(double x, double y){
		double[] b1 = arg1.izracunaj(x, y);
		double[] b2 = arg2.izracunaj(x, y);
		double[] nova = new double[3];
		for (int i = 0; i < nova.length; i += 1){
			nova[i] = 0.5 * (b1[i] + b2[i]);
		}
		return nova;
	}
}
