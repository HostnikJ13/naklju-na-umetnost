package Operatorji;


public class Boing extends Operator{
	Operator arg;
	
	private double boing(double x) {	
		return Math.abs(Math.sin(Math.pow(Math.abs(3*x), Math.abs(3*x))) / Math.pow(2, (Math.pow(Math.abs(3*x), Math.abs(3*x)) - Math.PI/2.5) / Math.PI));
	}
	
	public Boing(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = {boing(b[0]), boing(b[1]), boing(b[2])};
		return nova;
	}
}
