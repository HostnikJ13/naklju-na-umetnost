package Operatorji;

public class Na3 extends Operator{
	Operator arg;
	
	public double na3(double x) {
		return Math.pow(x, 3);
	}
	
	public Na3(int velikost){
		arg = Operator.naredi(velikost);
	}
	
	public double[] izracunaj(double x, double y){
		double[] b = arg.izracunaj(x, y);
		double[] nova = {na3(b[0]), na3(b[1]), na3(b[2])};
		return nova;
	}
}
