import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.text.NumberFormatter;


@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame implements ActionListener {

	private JButton gumbRisi;
	private JButton gumbShrani;
	private JButton gumbVelikost;
	private JComboBox<String> izbiraB;
	private SlikaPanel slika;
	private JLabel napisVisina;
	private JLabel napisSirina;
	private JFormattedTextField vnosVisina;
	private JFormattedTextField vnosSirina;
	private FileDialog fd;
	int sirinaMax;
	int visinaMax;
	
	public GlavnoOkno() {
		super();
		
		this.getContentPane().setBackground(new Color(235, 255, 255));
		
		
		// Dobimo višino in širino zaslona uporabnika.
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		
		sirinaMax = gd.getDisplayMode().getWidth();
		visinaMax = gd.getDisplayMode().getHeight();
		
		
		// pregledovalnik vnosa širine in višine
		NumberFormat format = NumberFormat.getIntegerInstance();
		format.setGroupingUsed(false);
		
		NumberFormatter visinaFormatter = new NumberFormatter(format);
		visinaFormatter.setValueClass(Integer.class);
//		visinaFormatter.setAllowsInvalid(false);
		visinaFormatter.setMinimum(1);
		visinaFormatter.setMaximum(visinaMax);
		
		NumberFormatter sirinaFormatter = new NumberFormatter(format);
		sirinaFormatter.setValueClass(Integer.class);
//		sirinaFormatter.setAllowsInvalid(false);
		sirinaFormatter.setMinimum(1);
		sirinaFormatter.setMaximum(sirinaMax);

		
		setTitle("Naključna umetnost");
		slika = new SlikaPanel(512, 512);	
		
		this.setLayout(new GridBagLayout());
		
		// slika
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		add(slika, c);
		
		// gumb za risanje
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 3;
		c.weightx = 1;
		c.weighty = (1/10);
		gumbRisi = new JButton("nariši");
		gumbRisi.addActionListener(this);
		add(gumbRisi, c);
		
		// gumb za shranjevanje
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 3;
		c.weightx = 1;
		c.weighty = (1/10);
		c.anchor = GridBagConstraints.WEST;
		gumbShrani = new JButton("shrani");
		gumbShrani.addActionListener(this); 
		add(gumbShrani, c);
		
		// nastavitve za barvo slike
		c = new GridBagConstraints();
		String[] tip = {"običajna", "črno-bela", "negativ", "temnejša", "svetlejša"};
		izbiraB = new JComboBox<>(tip);
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 1;
        c.weighty = (1/10);
        c.anchor = GridBagConstraints.EAST;
        izbiraB.addActionListener(this); 
        add(izbiraB, c);
        
        // napis za višino
        c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.EAST;
		napisVisina = new JLabel("višina (max " + visinaMax + ") :   ");
		add(napisVisina, c);
		
		// vnosno polje za višino
        c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.HORIZONTAL;
		vnosVisina = new JFormattedTextField(visinaFormatter);
		vnosVisina.setValue(512);
		vnosVisina.addActionListener(this);
		add(vnosVisina, c);
		
		// napis za širino
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.EAST;
		napisSirina = new JLabel("širina (max " + sirinaMax + ") :   ");
		add(napisSirina, c);
		
		// vnosno polje za širino
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.HORIZONTAL;
		vnosSirina = new JFormattedTextField(sirinaFormatter);
		vnosSirina.setValue(512);
		vnosSirina.addActionListener(this); 
		add(vnosSirina, c);
		
		// gumb za spremembo velikosti
		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 1;
		gumbVelikost = new JButton("spremeni velikost");
		gumbVelikost.addActionListener(this); 
		add(gumbVelikost, c);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// risanje
		if (e.getSource() == gumbRisi){
			izbiraB.setSelectedIndex(0);
			slika.narisi();
		} 
		
		// shranjevanje
		else if (e.getSource() == gumbShrani){
			fd = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
			fd.setFile("slika.png");
			fd.setVisible(true);
			String ime = fd.getFile();
			if (ime != null){
				File file = new File(fd.getDirectory(), fd.getFile());
				try {
					ImageIO.write(slika.slika, "png", file);
				} 
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		// sprememba velikosti
		else if (e.getSource() == gumbVelikost){
			int s = Integer.parseInt(vnosSirina.getText());
			int v = Integer.parseInt(vnosVisina.getText());
			slika.spremeniVelikost(s, v);
			this.pack();
			if (v > visinaMax-107){
				this.setExtendedState(JFrame.MAXIMIZED_BOTH);
			}
		}
		
		// sprememba barve
		else if (e.getSource() == izbiraB){
			if (izbiraB.getSelectedItem().toString().equals("črno-bela")){
				slika.crnoBelo(slika.slika);
			}
	        else if (izbiraB.getSelectedItem().toString().equals("običajna")){
	        	slika.barvno();
	        }
	        else if (izbiraB.getSelectedItem().toString().equals("negativ")){
	        	slika.negativ(slika.slika);
	        }
	        else if (izbiraB.getSelectedItem().toString().equals("temnejša")){
	        	slika.temna(slika.slika);
	        }
	        else if (izbiraB.getSelectedItem().toString().equals("svetlejša")){
	        	slika.svetla(slika.slika);
	        }
		}
	}
	}