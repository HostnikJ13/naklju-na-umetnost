import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JPanel;

import Operatorji.Operator;


@SuppressWarnings("serial")
public class SlikaPanel extends JPanel {
	public BufferedImage slika;
	public BufferedImage stara;
	private Thread slikarThread;
	private boolean ustaviSe;
	boolean jeStara;
	int w = 0;
	int h = 0;


	public SlikaPanel(int width, int height) {
		super();
		slika = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		w = width;
		h = height;
		jeStara = false;
	}
	
	public void spremeniVelikost(int width, int height) {
		if (slikarThread != null) {
			ustaviSe = true;
			try {
				slikarThread.join();
			} catch (InterruptedException e) {
//					e.printStackTrace();
			}
			slikarThread = null;
		}
		ustaviSe = false;
		slika = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		w = width;
		h = height;
	}
	
	public void barvno(){
		if (slikarThread == null){
			if (jeStara){
				slika = stara;
			}
			repaint();
		}
	}
	
	public void crnoBelo(BufferedImage s){
		if (slikarThread == null){
			if (!jeStara){
				stara = s;
				jeStara = true;
			}
			slika = new BufferedImage(w, h, BufferedImage.TYPE_USHORT_GRAY);
			Graphics g = slika.getGraphics();  
			g.drawImage(s, 0, 0, null);  
			g.dispose();
			repaint();
		}
	}
	
	public void negativ(BufferedImage s){
		if (slikarThread == null){
			if (!jeStara){
				stara = s;
				jeStara = true;
			}
			BufferedImage novaSlika = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			for (int y=0; y < slika.getHeight(); y += 1) {
				for (int x=0; x < slika.getWidth(); x += 1) {
					Color barva = new Color(slika.getRGB(x, y));
					int r = 255 - barva.getRed();
					int g = 255 - barva.getGreen();
					int b = 255 - barva.getBlue();
					Color novaBarva = new Color(r, g, b);
					novaSlika.setRGB(x, y, novaBarva.getRGB());
				}
			}
			slika = novaSlika;
			repaint();
		}
	}
	
	public void temna(BufferedImage s){
		if (slikarThread == null){
			if (!jeStara){
				stara = s;
				jeStara = true;
			}
			BufferedImage novaSlika = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			for (int y=0; y < slika.getHeight(); y += 1) {
				for (int x=0; x < slika.getWidth(); x += 1) {
					Color barva = new Color(slika.getRGB(x, y));
					Color novaBarva = barva.darker();
					novaSlika.setRGB(x, y, novaBarva.getRGB());
				}
			}
			slika = novaSlika;
			repaint();
		}
	}
	
	public void svetla(BufferedImage s){
		if (slikarThread == null){
			if (!jeStara){
				stara = s;
				jeStara = true;
			}
			BufferedImage novaSlika = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
			for (int y=0; y < slika.getHeight(); y += 1) {
				for (int x=0; x < slika.getWidth(); x += 1) {
					Color barva = new Color(slika.getRGB(x, y));
					Color novaBarva = barva.brighter();
					novaSlika.setRGB(x, y, novaBarva.getRGB());
				}
			}
			slika = novaSlika;
			repaint();
		}
	}
	
	public int rgb(double[] barvica) {
		int[] barva1 = {(int) (128 * (barvica[0] + 1)), (int) (128 * (barvica[1] + 1)), (int) (128 * (barvica[2] + 1))};
		int r = Math.max(0, Math.min(255, barva1[0]));
		int g = Math.max(0, Math.min(255, barva1[1]));
		int b = Math.max(0, Math.min(255, barva1[2]));
		Color barva = new Color(r, g, b);
		return barva.getRGB();
	}
	
	public void izracunajSliko() {
		Random r = new Random();
		int n = r.nextInt(161) + 10;
		System.out.print("\n");
		Operator op = Operator.naredi(n);
		for (int y=0; y < slika.getHeight(); y += 1) {
			if (ustaviSe) {
				return;
			}
			repaint();
			for (int x=0; x < slika.getWidth(); x += 1) {
				double u = x * 2.0/slika.getWidth() - 1;
				double v = y * 2.0/slika.getHeight() - 1;
				slika.setRGB(x, y, rgb(op.izracunaj(u, v)));
			}
		}
	}
	
	public void narisi() {
		if (slikarThread != null) {
			ustaviSe = true;
			try {
				slikarThread.join();
			} catch (InterruptedException e) {
//				e.printStackTrace();
			}
			slikarThread = null;
		}
		slika = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		jeStara = false;
		Runnable slikar = new Runnable() {
			public void run() {
				izracunajSliko();
				slikarThread = null;
				repaint();
			}
		};
		slikarThread = new Thread(slikar);
		ustaviSe = false;
		slikarThread.start();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(slika.getWidth(), slika.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int w = slika.getWidth();
		int h = slika.getHeight();
		int x = (this.getWidth() - w)/2;
		int y = (this.getHeight() - h)/2;
		g.drawImage(slika, x, y, w, h, Color.BLACK, null);
	}
	
}